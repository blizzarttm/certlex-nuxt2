import colors from 'vuetify/es5/util/colors'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - certlex2',
    title: 'certlex2',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        type: 'text/javascript'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.less'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    //authorization JWT
    '@nuxtjs/auth-next',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://be-dev-01.certlex.de/api/v1/'
  },
  
  auth: {
      strategies: {
        local: {
            scheme: 'refresh',
            localStorage: {
              prefix: 'auth.'
            },
            token: {
              prefix: 'access_token.',
              property: 'access_token',
              maxAge: 86400,
              type: 'Bearer'
            },
            refreshToken: {
              prefix: 'refresh_token.',
              property: 'refresh_token',
              data: 'refresh_token',
              maxAge: 60 * 60 * 24 * 5
            },
            user: {
              property: 'user',
              autoFetch: true
            },
            endpoints: {
              login: { url: 'auth/login', method: 'post'},
              refresh: { url: 'auth/refresh_token', method: 'post' },
              user: { url: 'auth/user', method: 'get' },
              logout: { url: 'auth/logout', method: 'post'}
            },
        }
      }
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      light: true,
      themes: {
        light: {
          primary: '#0f4186',
          secondary: '#b0bec5',
          anchor: '#8c9eff',
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  buildDir: '.nuxt',
  build: {
    publicPath: '.nuxt/dist/'
  }
}
