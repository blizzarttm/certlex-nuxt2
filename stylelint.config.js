module.exports = {
  customSyntax: 'postcss-html',
  extends: [
    //'stylelint-config-standard',
    //'stylelint-config-recommended-vue'
  ],
  "plugins": [
    "stylelint-selector-bem-pattern"
  ],
  "rules": {
    // ...
    "plugin/selector-bem-pattern": {
      "componentName": "[A-Z]+",
      "componentSelectors": {
        "initial": "^\\.{componentName}(?:-[a-z]+)?$",
        "combined": "^\\.combined-{componentName}-[a-z]+$"
      },
      "utilitySelectors": "^\\.util-[a-z]+$"
    },
    // ...
  }
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
}
